package com.epam.model;

import java.io.*;

public class CustomBuffer {

    static int mb = 1024 * 1024 * 50;
    static public byte[] bytes = new byte[mb];

    public long writeDateToFile() {
        long startmethod = System.currentTimeMillis();
        try {
            DataOutputStream ds = new DataOutputStream(new BufferedOutputStream(new FileOutputStream("file.txt")));
            for (int i = 0; i < bytes.length; i++) {
                bytes[(int) i] = 5;
            }
            ds.write(bytes);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return System.currentTimeMillis() - startmethod;
    }

    public long writeBytesToFile() {
        long startmethod = System.currentTimeMillis();
        try {
            FileOutputStream fw = new FileOutputStream("file2.txt");
            for (int i = 0; i < bytes.length; i++) {
                bytes[(int) i] = 5;
            }
            fw.write(bytes);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return System.currentTimeMillis() - startmethod;
    }

    public long readBytesFromFile() {
        long startmethod = System.currentTimeMillis();
        try {
            FileInputStream fi = new FileInputStream("file2.txt");
            int data = fi.read();
            while (data != -1) {
                data = fi.read();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }


        return System.currentTimeMillis() - startmethod;
    }

    public long readDataFromFile() {
        long startmethod = System.currentTimeMillis();
        try (DataInputStream di = new DataInputStream(new BufferedInputStream(new FileInputStream("file2.txt")))) {
            int data = di.read();
            while (data != -1) {
                data = di.read();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return System.currentTimeMillis() - startmethod;
    }
}
