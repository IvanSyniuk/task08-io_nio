package com.epam.model;


import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Scanner;

public class DirectoryIn {

    static Scanner sc =new Scanner(System.in);
    static Path path = FileSystems.getDefault().getPath(sc.next()).toAbsolutePath();

    public static void main(String[] args) {
    showDirectoryIn();
    }
    static public void showDirectoryIn (){
        try {
            Files.walk(Paths.get(path.toUri()))
//                    .filter(Files::isRegularFile)
                    .map(Path::getFileName)
                    .forEach(System.out::println);
        }catch (IOException e){
            e.printStackTrace();
        }

    }
    static void createDir(){
        Files.createDirectory()
    }
}
