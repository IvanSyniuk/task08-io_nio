package com.epam.view;

import com.epam.controller.BufferController;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Scanner;

public class View {
    static Logger logger = LogManager.getLogger(BufferController.class);
    static Scanner sc = new Scanner(System.in);
    BufferController bufferController;

    public void showMenu(){
        logger.info("");
        switch(sc.nextInt()){
            case 1:bufferController.writeDataToFile();
            case 2:bufferController.writeBytesToFile();
            case 3: bufferController.readDataFromFile();
            case 4:bufferController.readBytesFromFile();

        }
    }
}
