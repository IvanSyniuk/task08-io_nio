package com.epam.controller;

import com.epam.model.CustomBuffer;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class BufferController {
    static Logger logger = LogManager.getLogger(BufferController.class);
    CustomBuffer customBuffer = new CustomBuffer();

    public void writeBytesToFile(){
       logger.info(customBuffer.writeBytesToFile());
    }
    public void writeDataToFile(){
        logger.info(customBuffer.writeDateToFile());
    }
    public void readDataFromFile(){
        logger.info(customBuffer.readDataFromFile());
    }
    public void readBytesFromFile(){
        logger.info(customBuffer.readBytesFromFile());
    }
}
